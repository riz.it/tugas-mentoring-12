<?php
defined('BASEPATH') or exit('No direct script access allowed');

class FileModel extends CI_Model
{

    protected $table = 'files';
    protected $primary_key = 'id';
    protected $order_by = 'id';
    protected $order_by_type = 'ASC';
    protected $timestamps = TRUE;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insertBatch($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function delete($id)
    {
        $this->db->where($this->primary_key, $id);
        return $this->db->delete($this->table);
    }

    public function findAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function find($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

}
