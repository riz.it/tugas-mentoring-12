<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Tugas Mentoring 12</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    <style type="text/css">
        ::selection {
            background-color: #E13300;
            color: white;
        }

        ::-moz-selection {
            background-color: #E13300;
            color: white;
        }

        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
            text-decoration: none;
        }

        a:hover {
            color: #97310e;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 19px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }

        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        #body {
            margin: 0 15px 0 15px;
            min-height: 96px;
        }

        p {
            margin: 0 0 10px;
            padding: 0;
        }

        p.footer {
            text-align: right;
            font-size: 11px;
            border-top: 1px solid #D0D0D0;
            line-height: 32px;
            padding: 0 10px 0 10px;
            margin: 20px 0 0 0;
        }

        #container {
            margin: 10px;
            border: 1px solid #D0D0D0;
            box-shadow: 0 0 8px #D0D0D0;
        }
    </style>

</head>

<body>

    <?php echo form_open_multipart('assignment/do_upload'); ?>
    <div id="container">
        <h1>Handle File Upload</h1>

        <div id="body">
            <div class="">
                <?php if ($this->session->flashdata('errorDelete')) : ?>
                    <div class="">
                        <div class="alert alert-danger" role="alert">
                            <?= $this->session->flashdata('errorDelete'); ?>
                        </div>
                    </div>
                <?php elseif ($this->session->flashdata('successDelete')) : ?>
                    <div class="">
                        <div class="alert alert-success" role="alert">
                            <?= $this->session->flashdata('successDelete'); ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col">
                        <div class="">
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">File Name</th>
                                        <th scope="col">File Path</th>
                                        <th scope="col">Handle</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    <?php foreach ($files as $file) : ?>
                                        <tr>
                                            <th scope="row"><?= $no ?></th>
                                            <td><?= $file->file_name ?></td>
                                            <td><?= $file->file_path ?></td>
                                            <td><a href="<?= base_url() ?>assignment/delete/<?= $file->id ?>" class="btn btn-danger" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">Remove</a></td>
                                        </tr>
                                        <?php $no++ ?>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <hr>
                <span><b>Upload Your File</b></span>

                <div class="row mt-2">
                    <div class="col-sm-12 mb-1 col-lg-6 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-3">
                                    <label for="singleUploader" class="form-label">Single Uploader</label>
                                    <input accept=".pdf" class="form-control form-control-sm" name="singleUploader" id="singleUploader" type="file">
                                    <div id="singleHelpBlock" class="form-text">
                                        The PDF file must be restricted to not exceed 5 MB.
                                    </div>
                                </div>
                            </div>
                            <?php if ($this->session->flashdata('errorSingle')) : ?>
                                <div class="card-footer">
                                    <div class="alert my-auto alert-danger" role="alert">
                                        <?= $this->session->flashdata('errorSingle'); ?>
                                    </div>
                                </div>
                            <?php elseif ($this->session->flashdata('successSingle')) : ?>
                                <div class="card-footer">
                                    <div class="alert my-auto alert-success" role="alert">
                                        <?= $this->session->flashdata('successSingle'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-sm-12 mb-1 col-lg-6 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-3">
                                    <label for="multipleUploader" class="form-label">Multiple Uploader</label>
                                    <input accept=".png, .jpg, .jpeg" class="form-control form-control-sm" name="multipleUploader[]" multiple="multiple" id="multipleUploader" type="file">
                                    <div id="multipleHelpBlock" class="form-text">
                                        Allowed for png/jpg/jpeg file.
                                    </div>
                                </div>
                            </div>
                            <?php if ($this->session->flashdata('errorMultiple')) : ?>
                                <div class="card-footer">
                                    <div class="alert my-auto alert-danger" role="alert">
                                        <?= $this->session->flashdata('errorMultiple'); ?>
                                    </div>
                                </div>
                            <?php elseif ($this->session->flashdata('successMultiple')) : ?>
                                <div class="card-footer">
                                    <div class="alert  my-auto alert-success" role="alert">
                                        <?= $this->session->flashdata('successMultiple'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <p class="footer"> <button type="submit" class="btn mx-2 my-2 btn-primary" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">
                Upload
            </button></p>
    </div>
    </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

</body>

</html>