<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Assignment extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form'));
		$this->load->model('FileModel');
		$this->load->library(['upload', 'session']);
	}

	public function index()
	{
		$data = [
			'files' => $this->FileModel->findAll(),
		];
		$this->load->view('assignment', $data);
	}

	public function do_upload()
	{
		$document_data = array();
		$image_data = array();

		if (empty($_FILES['singleUploader']['name']) && empty($_FILES['multipleUploader']['name'][0])) {
			$this->session->set_flashdata('error', 'No file selected');
			redirect(base_url('Assignment'));
		}


		if (!empty($_FILES['singleUploader']['name'])) {
			$document_config['upload_path'] = 'public/uploads/pdf/';
			$document_config['allowed_types'] = 'pdf';
			$document_config['max_size'] = 5000;

			$this->upload->initialize($document_config);

			if ($this->upload->do_upload('singleUploader')) {
				$singleUploaded = [
					'file_name' => $this->upload->data('file_name'),
					'file_path' => 'public/uploads/pdf/' . $this->upload->data('file_name'),
				];

				if($this->FileModel->insert($singleUploaded)){
					$this->session->set_flashdata('successSingle', 'File uploaded successfully.');
				}

			} else {
				$this->session->set_flashdata('errorSingle', $this->upload->display_errors());
			}
		}

		$count = count($_FILES['multipleUploader']['name']);

		for ($i = 0; $i < $count; $i++) {
			if (!empty($_FILES['multipleUploader']['name'][$i])) {

				$_FILES['file']['name'] = $_FILES['multipleUploader']['name'][$i];
				$_FILES['file']['type'] = $_FILES['multipleUploader']['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES['multipleUploader']['tmp_name'][$i];
				$_FILES['file']['error'] = $_FILES['multipleUploader']['error'][$i];
				$_FILES['file']['size'] = $_FILES['multipleUploader']['size'][$i];

				$image_config['upload_path'] = 'public/uploads/images/';
				$image_config['allowed_types'] = 'jpeg|jpg|png';
				$image_config['max_size'] = '5000';
				$image_config['file_name'] = $_FILES['multipleUploader']['name'][$i];

				$this->upload->initialize($image_config);

				if ($this->upload->do_upload('file')) { 
					$multipleUploaded[] = [
						'file_name' => $this->upload->data('file_name'),
						'file_path' => 'public/uploads/images/' . $this->upload->data('file_name'),
					]; 
				} else {
					
				$this->session->set_flashdata('errorMultiple', $this->upload->display_errors());
				}
			}
		}

		if (!empty($multipleUploaded)) {
			if($this->FileModel->insertBatch($multipleUploaded)){	
				
				$this->session->set_flashdata('successMultiple', 'File(s) uploaded successfully.');
			}
		}

		redirect(base_url('Assignment'));
	}

	public function delete($id)
	{
		$data = $this->FileModel->find($id);
		$res = $this->FileModel->delete($id);
		if ($res) {
			unlink($data->file_path);
			$this->session->set_flashdata('successDelete', 'File deleted successfully.');
		} else {
			$this->session->set_flashdata('errorDelete', 'No file matches.');
		}
		redirect(base_url('Assignment'));
	}
}
