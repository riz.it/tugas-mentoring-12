<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form'));
	}

	public function index()
	{
		redirect('Assignment');
	}

	public function upload()
	{
		$this->load->view('upload', array('error' => ' ', 'data' => ' ' ));
	}

	public function do_upload()
	{
		$config['upload_path']          = 'public/uploads/';
		$config['allowed_types']        = 'xlsx';
		$config['max_size']             = 100;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('document')) {
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload', $error);
		} else {
			$data = array('upload_data' => $this->upload->data(), 'error' => '', 'data' => 'File uploaded successfully');

			$this->load->view('upload', $data);
		}
	}
}
